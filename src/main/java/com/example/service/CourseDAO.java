package com.example.service;

import java.util.List;

import com.example.model.CourseModel;

public interface CourseDAO {
	CourseModel selectCourse(String idCourse);
	
	List<CourseModel> selectAllCourses();
}
