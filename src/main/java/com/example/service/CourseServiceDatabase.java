package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.example.dao.CourseMapper;
import com.example.model.CourseModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Primary
public class CourseServiceDatabase implements CourseService 
{
    @Autowired
    private CourseMapper courseMapper;

	@Override
	public CourseModel selectCourse(String idCourse) {
    	log.info ("select course with id_course {}", idCourse);
        return courseMapper.selectCourse(idCourse);
	}

	@Override
	public List<CourseModel> selectAllCourses() {
        log.info ("select all courses");
        return courseMapper.selectAllCourses();
	}

	@Override
	public void addCourse(CourseModel course) {
		courseMapper.addCourse(course);

	}

	@Override
	public void deleteCourse(String idCourse) {
    	log.info ("course " + idCourse + " deleted");
    	courseMapper.deleteCourse(idCourse);;
	}

	@Override
	public void updateCourse(CourseModel course) {
    	log.info("course "+ course.getIdCourse() +" updated");
    	courseMapper.updateCourse(course);
	}

}
